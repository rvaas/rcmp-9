# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
from os import path
from statsmodels.stats.multitest import multipletests

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'chemotaxis_B16.csv'), index_col=False)
df = df.dropna(axis=0,how='any')
df["ECT"].replace({'pLEC':1,'imLEC':2,'MS1':3}, inplace=True)
df = df.groupby('Well').mean()
df["ECT"].replace({1:'pLEC',2:'imLEC',3:'MS1'}, inplace=True)

# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Value ~ C(Day) + Endothelial*ECT + ECT*DMOG'
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
anova_summary = sm.stats.anova_lm(fit, typ=3)

# Print output
print(anova_summary)
print(summary)

# Write output
with open("chemotaxis_B16_modelANOVA.txt", "w") as text_file:
	text_file.write(anova_summary.to_string())
with open("chemotaxis_B16_modelGLM.txt", "w") as text_file:
	text_file.write(summary.as_text())

formula = 'Value ~ C(Day) + Endothelial:ECT + DMOG*ECT'
model = smf.ols(formula, df)
fit = model.fit()
summary_ = fit.summary()
levels = [
	'Endothelial:ECT[imLEC]',
	'Endothelial:ECT[MS1]',
	'Endothelial:ECT[pLEC]',
	]
comparisons = [[a,b] for a, b in itertools.combinations(levels,2)]
f_contrast = ['{} - {}'.format(a[0],a[1]) for a in comparisons]
comparisons += levels
f_contrast += levels
f_contrast = ','.join(f_contrast)
f = fit.f_test(f_contrast)

contrasts = []
for comparison in comparisons:
	if isinstance(comparison, str):
		contrast = comparison
		contrasts.append(contrast)
	else:
		contrast = '{} = {}'.format(comparison[0],comparison[1])
		contrasts.append(contrast)

print(contrasts)
t_tests = fit.t_test(contrasts)

legend=''
g, corrected_pvalues, _, _ = multipletests(t_tests.pvalue, alpha=0.05, method='fdr_bh')
for ix, p in enumerate(corrected_pvalues):
	legend += 'c{}: {}\n\tBenjamini-Hochberg corrected p={}\n'.format(ix,contrasts[ix],p)

with open("chemotaxis_4T1_modelPosthoc.txt", "w") as text_file:
	text_file.seek(0)
	text_file.write(summary_.as_text())
	text_file.write('\n')
	text_file.write(f.__str__())
	text_file.write('\n\n')
	text_file.write(legend)
	text_file.write('\n')
	text_file.write(t_tests.__str__())
	text_file.write('\n')

# Print output
print(summary_)
print('\n')
print(f.__str__())
print('\n\n')
print(legend)
print('\n')
print(t_tests.__str__())
print('\n')
